/* jshint node: true */
'use strict';

var async = require('async');
var debug = require('debug')('manifest');
var getit = require('getit');
var mkdirp = require('mkdirp');
var path = require('path');
var fs = require('fs');
var reTrailingSlash = /\/$/;

/**
  # manifest

  1. This has nothing to do with HTML5 cache-manifest / appcache
  2. This is designed to make downloading multiple files from a remote
     web location (using [getit](https://github.com/DamonOehlman/getit)) via
     a simple manifest file, really bloody simple.

  ## Example Usage

  <<< examples/simple.js

  More documentation to come, in the meantime have a look at the
  test directory.

  ## Reference
**/

/**
  ### load(target, opts?, callback)

  Load a manifest from the specified location.

**/
var load = exports.load = function(target, opts, callback) {
  var basePath = (target || '').replace(reTrailingSlash, '');
  var manifestFile;
  
  // remap args if required
  if (typeof opts == 'function') {
    callback = opts;
    opts = {};
  }
  
  // ensure we have opts
  opts = opts || {};
  
  // initialise the manifest file name
  manifestFile = basePath + '/' + (opts.manifest || 'manifest');
  
  // get the manifest from the specified target path
  debug('downloading manifest from: ' + manifestFile);
  getit(manifestFile, opts, function(err, data) {
    callback(err, (data || '').split('\n'), basePath);
  });
};

/**
  ### download(target, opts?, callback)

  Download all the files specified in the manifest file from the specified
  location.

**/
exports.download = function(target, opts, callback) {
  // remap args if required
  if (typeof opts == 'function') {
    callback = opts;
    opts = {};
  }
  
  // initialise defaults
  opts.path = opts.path || process.cwd();
  
  // load the manifest from the target location
  load(target, opts, function(err, items, basePath) {
    if (err) {
      return callback(err);
    }
    
    async.forEach(
      items,
      function(item, itemCallback) {
        var targetFile = path.resolve(opts.path, item);

        // ensure the directory for the file exists
        mkdirp(path.dirname(targetFile), function(err) {
          if (err) {
            return itemCallback(err);
          }
          
          // download the file to the target
          getit(basePath + '/' + item, opts)
            .pipe(fs.createWriteStream(targetFile))
            .on('close', itemCallback);
        });
      },
      callback
    );
  });
};