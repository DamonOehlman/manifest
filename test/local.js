var manifest = require('../'),
    assert = require('assert'),
    path = require('path'),
    rimraf = require('rimraf'),
    testSrcPath = path.resolve(__dirname, 'mock'),
    testDstPath = path.resolve(__dirname, 'mockdist');
    
    
describe('local manifest tests', function() {
    before(rimraf.bind(null, testDstPath));
    
    it('should be able to load a local manifest', function(done) {
        manifest.load(testSrcPath, function(err, data, basePath) {
            assert.equal(basePath, testSrcPath);
            assert(data.indexOf('css/test.css') >= 0);
            assert(data.indexOf('js/test.js') >= 0);

            done();
        });
    });
    
    it('should be able to move files from the source to the target', function(done) {
        manifest.download(testSrcPath, { path: testDstPath }, function(err, data) {
            done(err);
        });
    });
});