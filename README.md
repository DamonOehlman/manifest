# manifest

1. This has nothing to do with HTML5 cache-manifest / appcache
2. This is designed to make downloading multiple files from a remote
   web location (using [getit](https://github.com/DamonOehlman/getit)) via
   a simple manifest file, really bloody simple.


[![NPM](https://nodei.co/npm/manifest.png)](https://nodei.co/npm/manifest/)

[![Build Status](https://img.shields.io/travis/DamonOehlman/manifest.svg?branch=master)](https://travis-ci.org/DamonOehlman/manifest) [![stable](https://img.shields.io/badge/stability-stable-green.svg)](https://github.com/dominictarr/stability#stable) [![Dependency Status](https://david-dm.org/DamonOehlman/manifest.svg)](https://david-dm.org/DamonOehlman/manifest) 

## Example Usage

```js
var manifest = require('manifest');

// download files from test.com 
// as specified in the manifest file "http://test.com/manifest"
// to the current working directory
manifest.download('http://test.com/', function(err) {
  // it's done
});
```

More documentation to come, in the meantime have a look at the
test directory.

## Reference

### load(target, opts?, callback)

Load a manifest from the specified location.

### download(target, opts?, callback)

Download all the files specified in the manifest file from the specified
location.

## License(s)

### MIT

Copyright (c) 2014 Damon Oehlman <damon.oehlman@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
